(* ::Package:: *)

DoEpsilon::usage="DoEpsilon uses the package epsilon to convert a matrix to the canonical form.
DoEpsilon[M] returns {T, M2}, such that M2 = Inverse[T].M.T - Inverse[T].D[T,x], where M2 is of canonical form.";


Needs["Epsilon`"];


Begin["`Private`epsilon`"];


Options[DoEpsilon]={"Shell"->"sh","Script"->"#!/bin/bash",
    "factorep-at"->-1,Order->0,PolyLog->"GPL","fuchsify"->True,"normalize"->True};

DoEpsilon[exp_,OptionsPattern[]]:=Block[{dir,block,script,epsilon,fea=ToString[OptionValue["factorep-at"]],tmp1,file,
      tf=False,rule,symb,fch=OptionValue["fuchsify"],nml=OptionValue["normalize"],ord=OptionValue[Order]},
    If[!MemberQ[$Packages,"Epsilon`"],Message[DoEpsilon::nl];Return[exp]];
    dir=FileNameJoin[{OptionValue[AmpRed,Directory],OptionValue[AmpRed,FileBaseName],"epsilon"}];
    If[!TrueQ[DirectoryQ[dir]],CreateDirectory[dir,CreateIntermediateDirectories->True]];
    SetDirectory[dir];
    CheckAbort[
        DeleteFile[FileNames["*"]];
        Put[exp/.eps->ToExpression["ep"],"matrix.m"];
        If[Run["epsilon-prepare matrix.m > matrix"]=!=0,ResetDirectory[];Print["\"epsilon-prepare\" failed."];Return[$Failed]];
        rule=StringCases[ReadString["matrix"],
            ("isqrt"~~RegularExpression["[1-9]+"])|("sqrt"~~RegularExpression["[1-9]+"])|
            ("r"~~RegularExpression["[1-9]+"])|("q"~~RegularExpression["[1-9]+"])|"i"]//DeleteDuplicates;
        rule=ToExpression[#]->ToExpression[StringReplace[#,{(a2:"isqrt"|"sqrt"|"q"|"r")~~Longest[a1__]:>a2<>"["<>a1<>"]"}]]&/@rule;
        symb=ToExpression/@{"isqrt","sqrt","q","r","i"};
        rule=rule/.{symb[[1]][a1_]:>I*Sqrt[a1],symb[[2]][a1_]:>Sqrt[a1],symb[[3]][a1_]:>(Sqrt[a1]+1)/2,
            symb[[4]][a1_]:>(I*Sqrt[a1]+1)/2,(symb[[5]]->_)->(symb[[5]]->I)};
        If[rule=!={},
            tf=True;
            tmp1="&(P="<>StringReplace[#,{"isqrt"~~Longest[a1__]:>"isqrt"<>a1<>"^2+"<>a1,"sqrt"~~Longest[a1__]:>"sqrt"<>a1<>"^2-"<>a1,
                "r"~~Longest[a1__]:>"r"<>a1<>"^2-r"<>a1<>"+(1+"<>a1<>")/4","q"~~Longest[a1__]:>"q"<>a1<>"^2-q"<>a1<>"+(1-"<>a1<>")/4",
                "i"->"i^2+1"}]<>",1)"&/@ToString/@First/@rule;
            file=OpenWrite["enable.fer"];
            WriteString[file,StringRiffle[tmp1,"\n"]];
            Close[file];
        ];
        block=ToExpression["EpsilonBlocks"][exp];
        epsilon=Flatten[{{"epsilon --timings",If[tf," --symbols "<>StringRiffle[ToString/@First/@rule,","],Unevaluated@Sequence[]],
            "--load matrix "<>StringRiffle[ToString/@block[[1]]]<>" --queue queue",
            If[tf,"--fermat enable.fer",Unevaluated@Sequence[]],
            If[fch,"--fuchsify ",""]<>If[nml,"--normalize",""]<>If[fea==="0",""," --factorep-at "<>fea]},
            {StringTemplate["--block `1` `2` "<>If[fch,"--fuchsify ",""]<>If[nml,"--normalize",""]<>If[fea==="0",""," --factorep-at "<>fea]<>
                " --left-fuchsify"][##],"--write epsilon_tmp --export transformation_tmp.m"}&@@@Rest[block]//Flatten,
            {If[nml,"--block 1 "<>ToString[Length[exp]],""]<>If[fea==="0",""," --factorep-at "<>fea,""],
            "--write epsilon --export transformation.m",If[ord===0,{},"--dyson dyson.m "<>ToString[ord]<>" "<>OptionValue[PolyLog]<>" mma"]},">","epsilon.log"}];
        script=OptionValue["Script"]<>"

"<>StringRiffle[epsilon," \\\n        "];
        file=OpenWrite["epsilon.sh"];
        WriteString[file,script];
        Close[file];
        If[#["ExitCode"]=!=0,Print[#["StandardError"]];ResetDirectory[];Return[$Failed]]&[RunProcess[{OptionValue["Shell"],"epsilon.sh"}]];
        tmp1=Append[#,If[ord===0,{},Get["dyson.m"]]//Normal]&[{Get["transformation.m"],ToExpression["EpsilonRead"]["epsilon",ToExpression["CheckFuchsian"]->fch,
            ToExpression["CheckEpsilon"]->fch&&nml]}]/.rule/.ToExpression["ep"]->eps;
    ,ResetDirectory[];Abort[]];
    ResetDirectory[];
    tmp1
];


End[];


Protect[DoEpsilon];
